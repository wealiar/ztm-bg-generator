var css = document.querySelector("h3");
var color1 = document.querySelector("#color1");
var color2 = document.querySelector("#color2");
var body = document.querySelector("#gradient");
var bgRandomizer = document.querySelector("button");

init();

function init() {
    setRandomValuesForInput();
    css.textContent = body.style.background + ";";
}

function setGradient() {
    body.style.background =
        "linear-gradient(to right, "
        + color1.value
        + ", "
        + color2.value
        + ")";

    css.textContent = body.style.background + ";";
}

function setGradientToGivenValues(color1List, color2List) {
    console.log("new");
    body.style.background =
        "linear-gradient(to right, "
        + "rgb( " 
        + color1List[0]
        + ", " 
        + color1List[1]
        + ", " 
        + color1List[2]
        + "), "
        + "rgb( " 
        + color2List[0]
        + ", " 
        + color2List[1]
        + ", " 
        + color2List[2]
        + "))"; 
    css.textContent = body.style.background + ";";
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

function setRandomValuesForInput(){
    var r1, g1, b1, r2, g2, b2;
    r1 = getRandomInt(255);
    g1 = getRandomInt(255);
    b1 = getRandomInt(255);
    r2 = getRandomInt(255);
    g2 = getRandomInt(255);
    b2 = getRandomInt(255);

    setGradientToGivenValues([r1,g1,b1],[r2,g2,b2]);
}

color1.addEventListener("input", setGradient);
color2.addEventListener("input", setGradient);
bgRandomizer.addEventListener("click", setRandomValuesForInput);